// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Pawn.h"
#include "NodePawn.generated.h"

UCLASS()
class NODEGAME_API ANodePawn : public APawn
{
	GENERATED_BODY()
private:
    int upState{};
    int downState{};
    int leftState{};
    int rightState{};
    int actionState{};
    
    void SetupInput();
    void ProcessInput();
    
    void ToggleUp();
    void ToggleDown();
    void ToggleLeft();
    void ToggleRight();
    void ToggleAction();

public:
	// Sets default values for this pawn's properties
	ANodePawn();
    
    // Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};
