// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "NodeGame.h"
#include "NodeGameGameMode.h"
#include "NodeGamePlayerController.h"

ANodeGameGameMode::ANodeGameGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// no pawn by default
    UE_LOG(LogTemp, Log, TEXT("Action"));

	DefaultPawnClass = NULL;
	// use our own player controller class
	PlayerControllerClass = ANodeGamePlayerController::StaticClass();
}
