// Fill out your copyright notice in the Description page of Project Settings.

#include "NodeGame.h"
#include "NodePawn.h"

#define STATE_ACTIVE  1
#define STATE_INACTIVE 0 

#define TOGGLE_STATE(state) {\
if (state == STATE_INACTIVE) \
state = STATE_ACTIVE;\
else \
state = STATE_INACTIVE;\
}

void ANodePawn::ToggleUp()
{
    UE_LOG(LogTemp, Log, TEXT("Up"));
    TOGGLE_STATE(upState);
}

void ANodePawn::ToggleDown()
{
    UE_LOG(LogTemp, Log, TEXT("Down"));
    TOGGLE_STATE(downState);
}

void ANodePawn::ToggleLeft()
{
    UE_LOG(LogTemp, Log, TEXT("Left"));
    TOGGLE_STATE(leftState);
}

void ANodePawn::ToggleRight()
{
    UE_LOG(LogTemp, Log, TEXT("Right"));
    TOGGLE_STATE(rightState);
}

void ANodePawn::ToggleAction()
{
    UE_LOG(LogTemp, Log, TEXT("Action"));
    TOGGLE_STATE(actionState);
}

// Sets default values
ANodePawn::ANodePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANodePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

void ANodePawn::ProcessInput()
{
    if (upState == STATE_ACTIVE) {
        UE_LOG(LogTemp, Log, TEXT("going up"));
    }
    
    if (downState == STATE_ACTIVE) {
        UE_LOG(LogTemp, Log, TEXT("going down"));
    }
    
    if (leftState == STATE_ACTIVE) {
        UE_LOG(LogTemp, Log, TEXT("going left"));
    }
    
    if (rightState == STATE_ACTIVE) {
        UE_LOG(LogTemp, Log, TEXT("going right"));
    }
    
    if (actionState == STATE_ACTIVE) {
        UE_LOG(LogTemp, Log, TEXT("doing action"));
    }
}

// Called every frame
void ANodePawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    ProcessInput();
}

// Called to bind functionality to input
void ANodePawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
    UE_LOG(LogTemp, Log, TEXT("Bound Stuff"));

	Super::SetupPlayerInputComponent(InputComponent);
    InputComponent->BindAction("ActionUp", IE_Pressed, this, &ANodePawn::ToggleUp);
    InputComponent->BindAction("ActionUp", IE_Released, this, &ANodePawn::ToggleUp);
    InputComponent->BindAction("ActionDown", IE_Pressed, this, &ANodePawn::ToggleDown);
    InputComponent->BindAction("ActionDown", IE_Released, this, &ANodePawn::ToggleDown);
    InputComponent->BindAction("ActionLeft", IE_Pressed, this, &ANodePawn::ToggleLeft);
    InputComponent->BindAction("ActionLeft", IE_Released, this, &ANodePawn::ToggleLeft);
    InputComponent->BindAction("ActionRight", IE_Pressed,  this, &ANodePawn::ToggleRight);
    InputComponent->BindAction("ActionRight", IE_Released, this, &ANodePawn::ToggleRight);
    InputComponent->BindAction("ActionAction", IE_Pressed,  this, &ANodePawn::ToggleAction);
    InputComponent->BindAction("ActionAction", IE_Released, this, &ANodePawn::ToggleAction);
}

