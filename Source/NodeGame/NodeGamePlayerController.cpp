// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "NodeGame.h"
#include "NodeGamePlayerController.h"

ANodeGamePlayerController::ANodeGamePlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
    EnableInput(this);
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
