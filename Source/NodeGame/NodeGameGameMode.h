// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "NodeGameGameMode.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ANodeGameGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ANodeGameGameMode(const FObjectInitializer& ObjectInitializer);
};



